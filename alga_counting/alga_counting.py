import cv2
import sys
import numpy as np
import copy

H_START = 0
S_START = 0
V_START = 0
RANGE_START = np.array([H_START, S_START, V_START])

H_END = 102
S_END = 255
V_END = 144
RANGE_END = np.array([H_END, S_END, V_END])

def find_range(HSV_img, range_start, range_end):
    mask = cv2.inRange(HSV_img, range_start, range_end)

    kernel = np.ones((3,3), np.uint8)
    dilated_image = cv2.dilate(mask, kernel,iterations=1)
    # eroded_image = cv2.erode(dilated_image, kernel,iterations=1)
    contours, hierarchy = cv2.findContours(dilated_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contours

def alga_counting(img, HSV_img):
    alga = find_range(HSV_img, RANGE_START, RANGE_END)
    img_copy = copy.copy(img)
    # contours_img = copy.copy(img)
    if len(alga) > 0:
        # cv2.drawContours(contours_img, alga, -1, (0,255,0), 3)
        for i in range(len(alga)):
            x,y,w,h = cv2.boundingRect(alga[i])
            cv2.rectangle(img_copy,(x,y),(x+w,y+h),(0,0,0),2)
            startX = x
            startY = y - 15 if y - 15 > 15 else y + 15
            cv2.putText(img_copy, str(i), (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
    return len(alga), img_copy
    # cv2.imshow("contours_img", contours_img)

img = cv2.imread(sys.argv[1])

HSV_img = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

quantity, result = alga_counting(img, HSV_img)
# img_con = cv2.hconcat([img, result])
# cv2.imshow('img_con', img_con)
# cv2.imwrite('pictures/result3.jpg', result)

cv2.imshow("origin img", img)
cv2.imshow("result", result)
print("Number of cells: ", quantity)
cv2.waitKey(0)
cv2.destroyAllWindows()