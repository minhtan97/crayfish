import cv2
import sys
import numpy as np

def nothing(x):
    pass

img = cv2.imread(sys.argv[1])

HSV_img = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

cv2.namedWindow('HSV_color_start')
cv2.moveWindow('HSV_color_start', 0, 100)  
cv2.createTrackbar("H", "HSV_color_start",0,179,nothing)
cv2.createTrackbar("S", "HSV_color_start",0,255,nothing)
cv2.createTrackbar("V", "HSV_color_start",0,255,nothing)

cv2.namedWindow('HSV_color_end')
cv2.moveWindow('HSV_color_end', 450, 100)  
cv2.createTrackbar("H", "HSV_color_end",0,179,nothing)
cv2.createTrackbar("S", "HSV_color_end",0,255,nothing)
cv2.createTrackbar("V", "HSV_color_end",0,255,nothing)

start = np.empty([3,1])
end = np.empty([3,1])

while(1):
    start[0] = cv2.getTrackbarPos("H", "HSV_color_start")
    start[1] = cv2.getTrackbarPos("S", "HSV_color_start")
    start[2] = cv2.getTrackbarPos("V", "HSV_color_start")

    end[0] = cv2.getTrackbarPos("H", "HSV_color_end")
    end[1] = cv2.getTrackbarPos("S", "HSV_color_end")
    end[2] = cv2.getTrackbarPos("V", "HSV_color_end")

    mask = cv2.inRange(HSV_img, start, end)
    res = cv2.bitwise_and(img, img, mask= mask)
    img_con = cv2.hconcat([img, HSV_img, res])
    cv2.namedWindow('img_con')
    # cv2.moveWindow('img_con', 0, 400) 
    cv2.imshow("img_con", img_con)
    # cv2.imshow('original image',img)
    # cv2.imshow('HSV format image',HSV_img)
    # cv2.imshow('mask',mask)
    # cv2.imshow('res',res)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()