# alga_counting
----
# Introduction
----
This project uses HSV color space to count number of cells (alga).
# Requirement
----
# Usage
----
## 1. Find range of alga:
Using [this script](https://bitbucket.org/minhtan97/crayfish/src/master/alga_counting/find_thresh.py) to find range of alga. You can adjust value by trackbar.
## 2. Alga counting:
Using [this script](https://bitbucket.org/minhtan97/crayfish/src/master/alga_counting/alga_counting.py) to count number of cells.
# Result
----
![Result 1](pictures/result1.jpg)
![Result 2](pictures/result2.jpg)
![Result 3](pictures/result3.jpg)
# References
----
