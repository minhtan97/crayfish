import os
from yolo import *
import pickle
import copy
from matplotlib import pyplot as plt
from data_processing import *
from random import randint
import sys


model = load_model('./weight/objdect10.h5', compile=False)


frame = cv2.imread(sys.argv[1])
img = frame/(255)
img = cv2.resize(img, (416, 416))
img = np.expand_dims(img, axis = 0)
img = np.array(img)

start = time.time()
y = model.predict(img)
print("Execute time = ", time.time() - start)

true_boxs = interpret_netout(img[0]*255, y[0])

img = draw_box_predict(img[0]*255, true_boxs)
img = np.array(img, dtype = np.uint8)

cv2.imshow("result", img)
cv2.waitKey(0)