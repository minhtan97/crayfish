# ph_calculate
----
# Introduction
----
This project uses HSV color space to detect range of PH (0->14).
# Requirement
----
# Usage
----
## 1. Find range of PH:
Using [this script](https://bitbucket.org/minhtan97/crayfish/src/master/ph_calculate/find_ph.py) to find range of PH. You can adjust value by trackbar.
![Find range](picture/find_range.png)
## 2. Detect range of PH:
Using [this script](https://bitbucket.org/minhtan97/crayfish/src/master/ph_calculate/cal_ph.py) to detect range of PH. You can adjust range of PH in [this script](https://bitbucket.org/minhtan97/crayfish/src/master/ph_calculate/thresh.py).
![Detect range](picture/detect_range.png)
# References
----
