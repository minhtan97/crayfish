import cv2
import sys
import numpy as np
from thresh import *
import copy

def find_range(HSV_img, ph_start, ph_end):
    mask = cv2.inRange(HSV_img, ph_start, ph_end)

    kernel = np.ones((3,3), np.uint8)
    dilated_image = cv2.dilate(mask, kernel,iterations=1)
    contours, hierarchy = cv2.findContours(dilated_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contours

def calculate_ph(img, HSV_img):
    ph0 = find_range(HSV_img, PH0_START, PH0_END)
    ph1 = find_range(HSV_img, PH1_START, PH1_END)
    ph2 = find_range(HSV_img, PH2_START, PH2_END)
    ph3 = find_range(HSV_img, PH3_START, PH3_END)
    ph4 = find_range(HSV_img, PH4_START, PH4_END)
    ph5 = find_range(HSV_img, PH5_START, PH5_END)
    ph6 = find_range(HSV_img, PH6_START, PH6_END)
    ph7 = find_range(HSV_img, PH7_START, PH7_END)
    ph8 = find_range(HSV_img, PH8_START, PH8_END)
    ph9 = find_range(HSV_img, PH9_START, PH9_END)
    ph10 = find_range(HSV_img, PH10_START, PH10_END)
    ph11 = find_range(HSV_img, PH11_START, PH11_END)
    ph12 = find_range(HSV_img, PH12_START, PH12_END)
    ph13 = find_range(HSV_img, PH13_START, PH13_END)
    ph14 = find_range(HSV_img, PH14_START, PH14_END)
    if len(ph0) > 0:
        contour = sorted(ph0, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "0", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph1) > 0:
        contour = sorted(ph1, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "1", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph2) > 0:
        contour = sorted(ph2, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "2", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph3) > 0:
        contour = sorted(ph3, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "3", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph4) > 0:
        contour = sorted(ph4, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "4", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph5) > 0:
        contour = sorted(ph5, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "5", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph6) > 0:
        contour = sorted(ph6, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "6", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph7) > 0:
        contour = sorted(ph7, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "7", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph8) > 0:
        contour = sorted(ph8, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "8", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph9) > 0:
        contour = sorted(ph9, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "9", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph10) > 0:
        contour = sorted(ph10, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "10", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph11) > 0:
        contour = sorted(ph11, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "11", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph12) > 0:
        contour = sorted(ph12, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "12", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph13) > 0:
        contour = sorted(ph13, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "13", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    elif len(ph14) > 0:
        contour = sorted(ph14, key = cv2.contourArea, reverse = True)[0]
        x,y,w,h = cv2.boundingRect(contour)
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
        startX = x
        startY = y - 15 if y - 15 > 15 else y + 15
        cv2.putText(img, "14", (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
    else:
        print("PH is out of range 0 -> 14.")


img = cv2.imread(sys.argv[1])
img_copy = copy.copy(img)
HSV_img = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

calculate_ph(img_copy, HSV_img)

cv2.imshow("img", img_copy)
cv2.waitKey(0)
cv2.destroyAllWindows()